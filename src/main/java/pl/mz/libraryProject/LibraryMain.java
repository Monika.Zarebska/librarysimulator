package pl.mz.libraryProject;

import pl.mz.libraryProject.library.Library;
import pl.mz.libraryProject.library.resources.Book;
import pl.mz.libraryProject.library.resources.Magazine;
import pl.mz.libraryProject.library.users.Lecturer;

public class LibraryMain {

    public static void main(String[] args) {

        Lecturer annaKowalskaLecturer = new Lecturer("Anna", "Kowalska");
        Lecturer annaKowalska2 = new Lecturer("Anna", "Kowalska");

        Book sezonBurz = new Book("Andrzej Sapkowki", "Sezon Burz");
        Book sezonBurz2 = new Book("Andrzej Sapkowki", "Sezon Burz");
        Book krewElfow = new Book("Andrzej Sapkowki", "Krew Elfów");
        Book biblia = new Book(null, "Biblia");


        Magazine magazine1 = new Magazine("2017/01", "Burda");
        Magazine magazine2 = new Magazine("2017/01", "Burda");
        Magazine magazine3 = new Magazine("2017/02", "Burda");
        Magazine magazine4 = new Magazine(null, "Burda");
        Magazine magazine5 = new Magazine("2019/02", "Focus");


        Library monikasLibrary = new Library();
        monikasLibrary.addUserToLibrary(annaKowalskaLecturer);
        monikasLibrary.addUserToLibrary(annaKowalskaLecturer);
        monikasLibrary.addUserToLibrary(annaKowalska2);

        System.out.println();
        System.out.println("Printing users:");
        monikasLibrary.printListOfUsers();


        monikasLibrary.addItemToLibrary(sezonBurz, sezonBurz2, krewElfow, biblia);
        System.out.println();
        System.out.println("Printing books:");
        monikasLibrary.printListOfBooks();

        monikasLibrary.addItemToLibrary(magazine1, magazine2, magazine3,magazine4);

        System.out.println();
        System.out.println("Printing magazines:");
        monikasLibrary.printListOfMagazines();

        monikasLibrary.rentItemToUser(sezonBurz, annaKowalska2);
        monikasLibrary.rentItemToUser(sezonBurz, annaKowalska2);
        monikasLibrary.rentItemToUser(magazine1, annaKowalskaLecturer);
        System.out.println();
        System.out.println("Printing books:");
        monikasLibrary.printListOfBooks();

        System.out.println();
        System.out.println("Printing magazines:");
        monikasLibrary.printListOfMagazines();

        System.out.println();
        monikasLibrary.rentItemToUser(sezonBurz, annaKowalska2);
        System.out.println("Printing books:");
        monikasLibrary.printListOfBooks();

        System.out.println();
        System.out.println("Printing magazines:");
        monikasLibrary.printListOfMagazines();

        monikasLibrary.rentItemToUser(magazine5, annaKowalska2);

        System.out.println();
        System.out.println("Export file");
        monikasLibrary.exportUsersWithItemsToFile("plik");

        System.out.println();
        System.out.println("Import file");
        monikasLibrary.importItemsFromFile("probny.csv");
        System.out.println("Printing books:");
        monikasLibrary.printListOfBooks();
        System.out.println("Printing magazines:");
        monikasLibrary.printListOfMagazines();
    }
}
