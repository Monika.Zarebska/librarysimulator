package pl.mz.libraryProject.library;

import pl.mz.libraryProject.library.resources.Book;
import pl.mz.libraryProject.library.resources.Item;
import pl.mz.libraryProject.library.resources.Magazine;
import pl.mz.libraryProject.library.users.Lecturer;
import pl.mz.libraryProject.library.users.Student;
import pl.mz.libraryProject.library.users.User;

import java.io.*;
import java.util.*;

public class Library {

    private Map<User, UUID> usersAndCardNumbers = new HashMap<>();
    private Map<Item, Integer> itemsAndQuantity = new HashMap<>();
    private Map<Item, Integer> itemsAndCurrentQuantity = new HashMap<>();
    private Map<UUID, List<Item>> cardsNrWithRentItems = new HashMap<>();

    public Library() {
    }

    public void addUserToLibrary(User... users) {
        for (User user : users) {
            if (usersAndCardNumbers.containsKey(user)) {
                System.out.println("This user already exists:" + user.printUserName());
            } else {
                UUID cardNumber = UUID.randomUUID();
                usersAndCardNumbers.put(user, cardNumber);
                cardsNrWithRentItems.put(cardNumber, new ArrayList<>());
            }
        }
    }

    public void addItemToLibrary(Item... items) {
        for (Item item : items) {
            if (itemsAndQuantity.containsKey(item)) {
                itemsAndQuantity.put(item, itemsAndQuantity.get(item) + 1);
                itemsAndCurrentQuantity.put(item, itemsAndCurrentQuantity.get(item) + 1);
            } else {
                itemsAndQuantity.put(item, 1);
                itemsAndCurrentQuantity.put(item, 1);
            }
        }
    }

    public boolean rentItemToUser(Item item, User user) {

        if (isUserRegistered(user) && checkIfItemIsAvailable(item, user) && checkIfUserCanRentMoreItems(user)) {
            itemsAndCurrentQuantity.put(item, itemsAndCurrentQuantity.get(item) - 1);

            UUID cardNumber = usersAndCardNumbers.get(user);
            List<Item> rentItems = cardsNrWithRentItems.get(cardNumber);
            rentItems.add(item);

            return true;
        } else {
            return false;
        }
    }

    private boolean checkIfUserCanRentMoreItems(User user) {
        UUID cardNumber = usersAndCardNumbers.get(user);
        List<Item> rentItems = cardsNrWithRentItems.get(cardNumber);
        if (rentItems.size() == 4 && user instanceof Student) {
            System.out.println("You " + user.printUserName() + " are a student, you cannot borrow more than 4 items.");
            return false;
        }
        if (rentItems.size() == 10 && user instanceof Lecturer) {
            System.out.println("You " + user.printUserName() + " are a lecturer, you cannot borrow more than 10 items.");
            return false;
        }
        return true;
    }

    private boolean isUserRegistered(User user) {
        if (usersAndCardNumbers.containsKey(user)) {
            return true;
        } else {
            System.out.println("You are not a user of this library.");
            return false;

        }
    }

    private boolean checkIfItemIsAvailable(Item item, User user) {
        if (itemsAndCurrentQuantity.containsKey(item)) {
            if (itemsAndCurrentQuantity.get(item) == 0) {
                System.out.println("You " + user.printUserName() + " cannot borow this item. There is none left.");
                return false;
            } else {
                return true;
            }
        } else {
            System.out.println("You " + user.printUserName() + " cannot borow this item. There is no such item in my library.");
            return false;
        }
    }

    public void printListOfMagazines() {
        for (Map.Entry<Item, Integer> itemWithQuantity : itemsAndQuantity.entrySet()) {
            if (itemWithQuantity.getKey() instanceof Magazine) {

                Magazine magazine = (Magazine) itemWithQuantity.getKey();
                Integer quantity = itemWithQuantity.getValue();
                Integer currentQuantity = itemsAndCurrentQuantity.get(magazine);

                String getMagazineData = magazine.getTitle() + ";" + magazine.getNumber() + ";" + quantity + ";" + currentQuantity;

                System.out.println(getMagazineData);
            }
        }
    }

    public void printListOfBooks() {
        for (Map.Entry<Item, Integer> itemWithQuantity : itemsAndQuantity.entrySet()) {
            if (itemWithQuantity.getKey() instanceof Book) {

                Book book = (Book) itemWithQuantity.getKey();
                Integer quantity = itemWithQuantity.getValue();
                Integer currentQuantity = itemsAndCurrentQuantity.get(book);

                String getBookData = book.getTitle() + ";" + book.getAuthor() + ";" + quantity + ";" + currentQuantity;

                System.out.println(getBookData);
            }
        }
    }

    public void printListOfUsers() {
        for (Map.Entry<User, UUID> mapUsersWithCard : usersAndCardNumbers.entrySet()) {

            UUID cardNumber = mapUsersWithCard.getValue();
            User userName = mapUsersWithCard.getKey();
            String userData = userName.getFirstName() + ";" + userName.getLastName() + ";" + cardNumber + ";" + userName.getUserType();

            System.out.println(userData);
        }
    }

    public void importItemsFromFile(String csvFile) {
        try (BufferedReader br
                     = new BufferedReader(new FileReader(new File(csvFile)))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] dataFromFile = line.split(";");
                int quantity = Integer.parseInt(dataFromFile[2]);
                String itemType = dataFromFile[3];
                Item item;

                switch (itemType) {
                    case "B":
                        item = new Book(dataFromFile[1], dataFromFile[0]);
                        break;
                    case "M":
                        item = new Magazine(dataFromFile[1], dataFromFile[0]);
                        break;
                    default:
                        System.out.println("Invalid format. Create this line one more time: " + line);
                        continue;
                }
                for (int i = 1; i <= quantity; ++i) {
                    addItemToLibrary(item);
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void exportUsersWithItemsToFile(String csvFile) {

        List<String> baseToExportList = baseToExportList();

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFile + ".csv"))) {

            for (String baseToExport : baseToExportList) {
                writer.write(baseToExport);
                writer.newLine();
            }

        } catch (IOException exception) {
            exception.printStackTrace();

        }
    }

    private List<String> baseToExportList() {
        List<String> baseToExport = new ArrayList<>();

        for (Map.Entry<UUID, List<Item>> cardWithRentItems : cardsNrWithRentItems.entrySet()) {
            UUID cardNumber = cardWithRentItems.getKey();
            List<Item> itemList = cardWithRentItems.getValue();

            if (itemList.size() > 0) {
                String itemByText = "";
                for (Item item : itemList) {
                    if (item instanceof Book) {
                        Book book = (Book) item;
                        String bookAsText = book.getTitle() + "-" + book.getAuthor();
                        itemByText = itemByText + "; " + bookAsText;
                    } else if (item instanceof Magazine) {
                        Magazine magazine = (Magazine) item;
                        String magazineAsText = magazine.getTitle() + "-" + magazine.getNumber();
                        itemByText = itemByText + "; " + magazineAsText;
                    }
                }
                String text = cardNumber + "[" + itemByText.substring(2) + "]";
                System.out.println(text);
                baseToExport.add(text);
            }
        }
        return baseToExport;
    }
}