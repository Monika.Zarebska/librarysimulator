package pl.mz.libraryProject.library.users;

public class Student extends User {

    public Student(String firstName, String lastName) {
        super(firstName, lastName, "S");
    }
}