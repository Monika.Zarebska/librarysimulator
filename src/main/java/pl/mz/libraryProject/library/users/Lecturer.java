package pl.mz.libraryProject.library.users;

public class Lecturer extends User {

    public Lecturer(String firstName, String lastName) {
        super(firstName, lastName, "L");
    }
}
