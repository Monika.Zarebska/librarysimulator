package pl.mz.libraryProject.library.users;

public abstract class User {

    private String firstName;
    private String lastName;
    private String userType;

    public User(String firstName, String lastName, String userType) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userType = userType;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUserType() {
        return userType;
    }

    public String printUserName() {
        return firstName + " " + lastName;
    }
}
