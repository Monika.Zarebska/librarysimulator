package pl.mz.libraryProject.library.resources;

import java.util.Objects;

public class Magazine extends Item {

    // zalozylam ze magazyn moze nie miec numeru (np. "wydanie specjalne")
    private String number;
    private String title;

    public Magazine(String number, String title) {
        this.number = number;
        this.title = title;
    }

    public String getNumber() {
        if (number == null) {
            return "";
        }
        return number;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Magazine magazine = (Magazine) o;
        return Objects.equals(number, magazine.number) &&
                title.equals(magazine.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, title);
    }
}
